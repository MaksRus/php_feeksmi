<?php

use App\Post\Post;
use App\Comment\Comment;

function getDatabase()
{
    $posts = [
        new Post('Titre 1', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Post('Titre 2', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Post('Titre 3', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Post('Titre 4', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Post('Titre 5', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
    ];

    $comments = [
        new Comment('Moi', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Comment('Pas moi', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Comment('Lui', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.')
        new Comment('Encore lui', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
        new Comment('Ielle', 'Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.'),
    ];

    // Je mets mes commentaires dans les Posts
    $comments[0]->addComment($posts[1]);
    $comments[0]->addComment($posts[0]);
    $comments[1]->addComment($posts[2]);
    $comments[1]->addComment($posts[3]);
    $comments[2]->addComment($posts[4]);

    return $posts; 
}
