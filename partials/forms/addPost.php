<!-- Formulaire d'ajout de nouveaux articles -->

<!-- Le traitement se fait sur la même page donc on met le "/" pour rediriger sur l'index -->
<form action="/" method="get">
    <label for="title">Titre de l'article</label>
    <input id="title" type="text" name="title">

    <label for="contenu">Contenu</label>
    <textarea id="contenu" name="contenu" cols="30" rows="10"></textarea>
</form>
