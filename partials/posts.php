<!-- Section avec tous les articles -->
<h1>TOUS LES ARTICLES</h1>

<?php foreach ($posts as $post) ?>
<article>
    <h2><?= $post['title'] ?></h2>
    <small>Publié le <?= $post['createdAt'] ?></small>
    <div class="article-content">
        <?= $post['content'] ?>
    </div>
</article>
    <aside>
        <h3>Section commentaires</h3>
    <?php foreach ($post['comments'] as $comment): ?>
<strong>Message de <?=$post['author'] ?>, posté le <?php $post['content'] ?></strong>
        <div><?=$post['createdAt'] ?></div>
    <?php endforeach ?>
    </aside>
<?php endforeach ?>
