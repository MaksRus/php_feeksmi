<?php

namespace App\Post;

use DateTime;
use App\Post\Comment;

class Post {

    public function __construct(
        private string $title,
        private string $content,
        private DateTime $createdAt = new DateTime(),
        private Comment[] $comments = [],
    ) {}


    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getComments(): array
    {
        return $this->comments;
    }

    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;
    }
}
