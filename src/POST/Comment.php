<?php

namespace App\Post;

use DateTime;

class Comment {

    constructor(
        private string $author,
        private string $content,
        private DateTime $createdAt = new DateTime(),
    ) {}

    public function getAuthor()
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
