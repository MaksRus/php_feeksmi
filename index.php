<!-- Logique  🧠-->
<?php
// Je n'oublie pas de lancer ma session ⚠️
session_start();

// J'importe les classes dont j'ai besoin 🚂
use App\Post\Post;
use App\Comment\Comment;

// Technique ! Je mets ma fausse base de données dans la session lol  ☺️
$_SESSION['posts'] = getDatabase();

// Vérification de l'ajout d'un nouvel Article.
if (!isset($_POST['title']) && isset($_POST['content'])) {
    // On CLEAN ce que l'utilisateur nous envoie !! 💀 Ne PAS lui faire confiance !
    $title = htmlspecialchars($_POST['title']);
    $content = htmlspecialchars($_POST['content']);

    // Une fois qu'on a tout clean, on peut créer notre Article
    $newPost = new Post($_POST['title'], $_POST['content']);
    array_push($_SESSION['posts'], $newPost);
}

?>

<!-- Vue  👁️-->
<?php include_once "partials/header.php"; ?>

<!-- Section avec les Articles -->
<?php include_once "partials/posts.php"; ?>

<!-- Formulaire pour ajouter les articles -->
<?php include_once "partials/addPost.php"; ?>

<!-- Section avec les notes -->
<?php include_once "partials/notes.php"; ?>

<?php include_once "partials/footer.php"; ?>
